var WebSocketServer = require('ws').Server
var wss = new WebSocketServer({port: 4200})

var wsList = []
console.log("socket bağlandı")

wss.on('connection', function (ws) {
  console.log('WS connection established!')
  wsList.push(ws)

  ws.on('close', function () {
    wsList.splice(wsList.indexOf(ws), 1)
    console.log('WS closed!')
  })

  ws.on('message', function (message) {
    console.log('got ws message: ' + message)
    // send to everybody on the site
    wsList.forEach(function (ws) {
      ws.send(message)
    })
  })
})
