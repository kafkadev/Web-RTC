//G:\laragon\www\webrtc\Master\serverless-webrtc-xxUygun


document.getElementById("startCamera").addEventListener("click", startCamera);
document.getElementById("createOffer").addEventListener("click", createOffer);
document.getElementById("setAnswer").addEventListener("click", setAnswer);
document.getElementById("createAnswer").addEventListener("click", createAnswer);
document.getElementById("mesajGonder").addEventListener("click", mesajGonder);

var sdpConstraints = {
  optional: [],
  mandatory: {
    OfferToReceiveAudio: true,
    OfferToReceiveVideo: true
  }
}


var pc0 = null
var pc1 = null
var pc2 = null
var dc1 = null
var tn1 = null
var activedc = null
var streamCamera = null
var connection = null
var cfg = {'iceServers': [{'url': 'stun:23.21.150.121'}]},
con = { 'optional': [{'DtlsSrtpKeyAgreement': true}] }
var pc1 = new RTCPeerConnection(cfg, con)

//kamera Başlatır
function startCamera(){
  navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia ||
  navigator.mozGetUserMedia ||
  navigator.msGetUserMedia
  navigator.getUserMedia({video: true, audio: true}, function (stream) {
    var video = document.getElementById('localVideo')
    video.src = window.URL.createObjectURL(stream)
    video.play()
    streamCamera = stream
   socketBaglan()
   setupOffer()
 }, function (error) {
  console.log('stream hata: ' + error)
})

}

//Teklifi Yapan PC

function createOffer(){
  dc1 = pc1.createDataChannel('test', {reliable: true})
  activedc = dc1
  dc1.onopen = function(e) {
    console.log(e)
  }
  dc1.onmessage = function(e) {
    console.log(e)
  }
  pc1.createOffer(function(desc) {
    pc1.setLocalDescription(desc, function() {}, function() {})
    document.getElementById('localOffer').value = JSON.stringify(desc, null, 2)
  }, function() { }, sdpConstraints)
}

function setupOffer(){
  pc1.addStream(streamCamera)


  pc1.onaddstream = handleOnaddstream

}




function setAnswer(){

  var CEVAP = document.getElementById('getAnswer').value
  var CEVAPSET = new RTCSessionDescription(JSON.parse(CEVAP));
  pc1.setRemoteDescription(CEVAPSET);
//document.getElementById('setOfferValue').value = JSON.stringify(desc, null, 2)

}

function createAnswer(){

  var offer = document.getElementById('pc1Offer').value
  var offerDesc = new RTCSessionDescription(JSON.parse(offer))
  pc1.setRemoteDescription(offerDesc)
  pc1.createAnswer(function(answerDesc) {
    pc1.setLocalDescription(answerDesc)
  },
  function () { },
  sdpConstraints)
  pc1.onicecandidate = function (e) {
    if (e.candidate == null) {
      //pc1.addIceCandidate(new RTCIceCandidate(event.candidate));
      document.getElementById('sentAnswer').value = JSON.stringify(pc1.localDescription, null, 2)
    }
  }
  pc1.ondatachannel = function (e) {
    var datachannel = e.channel || e;
    dc2 = datachannel
    activedc = dc2
    dc2.onopen = function (e) {
      console.log(e)
    }
    dc2.onmessage = function (e) {
      console.log(e)
    }
  }
}


function mesajGonder() {
  activedc.send(JSON.stringify({message: (new Date())+' gidiyor...'}));
}

function handleOnaddstream (e) {
  console.log('Got remote stream', e.stream)
  var el = document.getElementById('remoteVideo')
  el.autoplay = true
  attachMediaStream(el, e.stream)
}



function socketBaglan(){
    var wsUrl = "ws://localhost:4200"
    connection = new WebSocket(wsUrl);

    connection.onopen = function(event){
      console.log((new Date())+' Bağlantı Başarılı');
    };


    connection.onerror = function(error){
      console.log((new Date())+' WebSocket hatası: ');
      console.log(error);
    };


    connection.onclose = function(event){
      console.log((new Date())+' Socket Kapandı');
    };


    connection.onmessage = function(message){
      try {
        var data = JSON.parse(message);
      } catch (e) {
        console.log('valid JSON');
        console.log(message);
        return;
      }

    };
  };
